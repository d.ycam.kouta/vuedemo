import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ProductList from '../views/productSearch.vue'
import VuetifyGrid from '../components/common/vuetifyGrid/vuetifyGrid'
import SelectBox from '../components/common/textbox/selectbox'
import Tenkey from '../components/common/textbox/textbox-tenkey'
import TextBox from '../components/common/textbox/textbox'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/productSearch',
        name: 'ProductSearch',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: ProductList
    },

    {
        path: '/vuetifyGrid',
        name: 'vuetifyGrid',
        component: VuetifyGrid
    },
    {
        path: '/selectbox',
        name: 'selectbox',
        component: SelectBox
    },
    {
        path: '/textbox',
        name: 'textbox',
        component: TextBox
    },
    {
        path: '/tenkey',
        name: 'tenkey',
        component: Tenkey
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router